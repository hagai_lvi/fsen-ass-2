import junit.framework.TestCase;

import java.util.Date;
import java.util.List;

/**
 * Created by hagai_lvi on 12/5/14.
 */
public class PaisTests extends TestCase {

    public static final String REAL_INTERFACE_CLASS_NAME = "PaisRealInterface";
    public static final String PROXY_INTERFACE = "proxy";
    private PaisTestInterface testInf;


    /**
     * In order to run the real test set an env variable INTERFACE = {@link PaisTests#REAL_INTERFACE_CLASS_NAME}
     */
    public PaisTests(String name) {
        super(name);
        if (REAL_INTERFACE_CLASS_NAME.equals(System.getenv("INTERFACE")))
            setTestInterface(REAL_INTERFACE_CLASS_NAME);
        else
            setTestInterface(PROXY_INTERFACE);
    }



    public void setTestInterface(String interfaceType){
        if (interfaceType.equals(REAL_INTERFACE_CLASS_NAME))
            try {
                testInf = (PaisTestInterface) Class.forName(REAL_INTERFACE_CLASS_NAME).newInstance();
            } catch (Exception e) {
                e.printStackTrace();
            }
        else if (interfaceType.equals(PROXY_INTERFACE))
            testInf = new PaisTestInterface();
        else
            fail("Undefined interface " + interfaceType);
    }

    public boolean login(String id, String psw){

        return testInf.login(id, psw);
    }

    public boolean addNewShow(String name, String desc, String room, Date date, int price, Date finalReg ){
        return testInf.addNewShow(name, desc, room, date, price, finalReg);
    }


    public boolean searchForShow(String showname){
        return testInf.searchForShow(showname);
    }

    public boolean makeReservation(String showName, String phone, String seats){
        return testInf.makeReservation(showName, phone, seats);
    }

    public List<String> getAvaiableSeats(String showName){
        return testInf.getAvailableSeats(showName);
    }

}


