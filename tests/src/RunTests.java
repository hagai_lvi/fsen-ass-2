import org.junit.runner.RunWith;
import org.junit.runners.Suite;

/**
 * Created by hagai_lvi on 12/15/14.
 */

@RunWith(Suite.class)
@Suite.SuiteClasses({
        TestAddNewShow.class,
        TestOrderSeats.class
})
public class RunTests {
}
