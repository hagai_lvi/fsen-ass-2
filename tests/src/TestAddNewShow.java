/**
 * Created by xkcd2 on 12/5/2014.
 */

import junit.*;
import junit.framework.Assert;

import java.sql.Date;

public class TestAddNewShow extends PaisTests {


    public TestAddNewShow(){
        super("TestAddNewShow");
    }


    private void reset(){

    }


    public void testAddNewShowValid() {
        Assert.assertTrue(login("tom", "password"));   //good
        Assert.assertTrue(addNewShow("Fidler on the roof", "very good show", "101", new Date(12312321), 250, new Date(3123124)));  //good
        reset();
    }


    public void testAddNewShowInvalidDetails() {

        Assert.assertTrue(login("tom", "password"));
        Assert.assertTrue(addNewShow("Fidler on the roof", "", "101", new Date(12312321), 250, new Date(3123124)));  //bad
        reset();

        Assert.assertTrue(login("tom", "password"));
        Assert.assertFalse(addNewShow("Fidler on the roof", "nice show", "", new Date(12312321), 250, new Date(3123124)));  //bad
        reset();

        Assert.assertTrue(login("tom", "password"));
        Assert.assertFalse(addNewShow("", "nice show", "101", new Date(12312321), 250, new Date(3123124)));  //bad
        reset();

        Assert.assertTrue(login("tom", "password"));
        Assert.assertFalse(addNewShow("", "nice show", "101", new Date(12312321), 250, new Date(3123124)));  //bad
        reset();
    }

    public void testAddNewShowNonExistingRoom() {


        Assert.assertTrue(login("tom", "password"));
        Assert.assertFalse(addNewShow("Fidler on the roof", "very good show", "thereIsNoSuchRoom", new Date(12312321), 250, new Date(3123124)));
        reset();
    }


    public void testAddNewShowDuplicateBooking(){

        Assert.assertTrue(login("tom", "password"));
        Assert.assertTrue(addNewShow("Hamlet", "good show", "101", new Date(12312321), 300, new Date(10)));
        Assert.assertFalse(addNewShow("Fidler on the roof", "very good show", "101", new Date(12312321), 250, new Date(3123124))); // should fail, room already orderead
        reset();

    }

    public void testManagerLogin(){


        Assert.assertFalse(login("admin", ""));   //bad
        reset();

        Assert.assertFalse(login("manager","1234"));    //bad
        reset();


    }

    public void testDefaultPass(){

        Assert.assertFalse(login("admin", "admin")); //good
        Assert.assertFalse(login("admin", "1234")); //bad
        Assert.assertFalse(login("manager","@@#!$@#$#@$@#")); //sad
    }



}
