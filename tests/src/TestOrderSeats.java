import junit.framework.Assert;
import org.junit.Test;

import java.util.Calendar;
import java.util.Date;
import java.util.List;

/**
 * Created by hagai on 07/12/14.
 */
public class TestOrderSeats extends PaisTests {

    private static final String SHOW_NAME = "Romeo and Juliet";
    private static final Date TODAY = new Date(System.currentTimeMillis());
    private static final String VALID_PHONE = "03-5555555";
    private static final String VALID_SEATS = "A1";

    public TestOrderSeats(String name) {
        super(name);
    }

    /*************************
    * Tests for user stories *
    **************************/

    @Test
    public void testExistingShow(){
        addTestShow();
        Assert.assertNotNull(searchForShow(SHOW_NAME));//success
        Assert.assertNull(searchForShow("NON_EXISTENT_SHOW"));
    }

    @Test
    public void testInsertCustomerDetailes(){
        Assert.assertTrue(makeReservation(SHOW_NAME,VALID_PHONE,VALID_SEATS)); //success

        Assert.assertFalse(makeReservation("NON_EXISTENT_SHOW",VALID_PHONE,VALID_SEATS));
        Assert.assertFalse(makeReservation(SHOW_NAME,"03-INVALID_PHONE",VALID_SEATS));
        Assert.assertFalse(makeReservation(SHOW_NAME,VALID_PHONE,"NON_VALID_SEATS"));
        Assert.assertFalse(makeReservation("NON_EXISTENT_SHOW","03-INVALID_PHONE","NON_VALID_SEATS"));


    }



    /*******************************
    * Tests for hidden assumptions *
    ********************************/

    @Test
    public void testShowMoreSeatsToPaisClubMembers(){
        List<String> regularSeats = getAvaiableSeats(SHOW_NAME);
        login("Pais-user", "Pais-usr-password");
        List<String> paisSeats = getAvaiableSeats(SHOW_NAME);
        Assert.assertTrue(paisSeats.containsAll(regularSeats));
        Assert.assertFalse(regularSeats.containsAll(paisSeats));
    }


    /**
     * Tests a scenario in which two users try to order the same seats simultaneously
     * //fail
     */
    @Test
    public void testOrderMultipleBuyers() throws Exception {
        final Error[] t1Error = {null}, t2Error = {null};

        addTestShow();
        Thread buyer1 = new Thread(new Runnable() {
            @Override
            public void run() {
                try {
                    boolean a2 = makeReservation(SHOW_NAME, "03-22222222", "A2");
                    Assert.assertTrue(a2);
                    Assert.assertFalse(true);
                }catch (Error e){
                    t1Error[0] = e;
                }
            }
        });

        Thread buyer2 = new Thread(new Runnable() {
            @Override
            public void run() {
                try {
                    Assert.assertFalse(makeReservation(SHOW_NAME, "03-3333333", "A2"));
                }catch (Error e){
                    t2Error[0] = e;
                }
            }
        });
        buyer1.start();
        buyer2.start();

        buyer1.join();
        buyer2.join();

        if(t1Error[0] != null)
            throw t1Error[0];

        if(t2Error[0] != null)
            throw t2Error[0];
    }

    /**
     * Tests a scenario in which one user try to order seats
     * //success
     */
    @Test
    public void testOrderSingleBuyer() throws InterruptedException {
        final Error[] t1Error = {null};
        addTestShow();
        Thread buyer1 = new Thread(new Runnable() {
            @Override
            public void run() {
                try {
                    Assert.assertTrue(makeReservation(SHOW_NAME, "03-22222222", "A1"));
                }catch (Error e){
                    t1Error[0] = e;
                }
            }
        });
        buyer1.start();

        buyer1.join();

        if (t1Error[0] != null){
            throw t1Error[0];
        }
    }

    private void addTestShow() {
        Calendar c = Calendar.getInstance();
        c.setTime(TODAY);
        c.add(Calendar.DATE,1);
        Date regDate = c.getTime();
        c.add(Calendar.DATE,1);
        Date showDate = c.getTime();

        addNewShow(SHOW_NAME, "Great show", "Hall 1", regDate, 100, showDate);
    }
}
